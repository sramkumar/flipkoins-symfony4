var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/css/main.less')
    .addPlugin(new CopyWebpackPlugin([
        // copies to {output}/static
        { from: './assets/images', to: 'images' },
        { from: './assets/animate.css', to: 'animate' },
        { from: './assets/bootstrap-carousel-swipe', to: 'carousel' },
        { from: './assets/bootstrap-material-design-font', to: 'material-design' },
        { from: './assets/mobirise', to: 'mobirise' },
        { from: './assets/progressbar', to: 'progressbar' },
        { from: './assets/bootstrap', to: 'bootstrap' },
        { from: './assets/tether', to: 'tether' },
        { from: './assets/viewport-checker', to: 'viewport-checker' },
        { from: './assets/dropdown', to: 'dropdown' },
        { from: './assets/smooth-scroll', to: 'smooth-scroll' },
        { from: './assets/theme', to: 'theme' },
        { from: './assets/touch-swipe', to: 'touch-swipe' },
        { from: './assets/jquery-mb-ytplayer', to: 'player' },
        { from: './assets/mobirise-slider-video', to: 'mobirise-slider-video' },
        { from: './assets/socicon', to: 'socicon' },
        { from: './assets/jarallax', to: 'jarallax' },
        { from: './assets/formoid', to: 'formoid' },
        { from: './assets/web', to: 'web' },
    ]))
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    .enableLessLoader()

    // uncomment to define the assets of the project
    // .addEntry('js/app', './assets/js/app.js')

    // uncomment if you use Sass/SCSS files
    // .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    // .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
