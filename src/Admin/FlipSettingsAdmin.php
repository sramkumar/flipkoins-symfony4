<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 4/11/15
 * Time: 11:03 AM
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FlipSettingsAdmin extends AbstractAdmin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('profitabilityValue')
            ->add('adsTimeInterval', null, array('label'=>'Stream Watching Time Intervel (In seconds)'))
            ->add('adsTimeInterval')
            ->add('adClickTimeInterval', null, array('label'=>'Stream Add Click Time Intervel (In hours)'))
            ->add('adClickTicketEarning')
            ->add('referralOwnerEarning', null, array('label'=>'Referral owner earnings'))
            ->add('referralEarning')

        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('profitabilityValue')
            ->add('adsTimeInterval')
            ->add('adsTimeInterval')
            ->add('referralOwnerEarning')
            ->add('adClickTimeInterval')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('profitabilityValue')
            ->add('adsTimeInterval')
            ->add('adsTimeInterval')
            ->add('referralOwnerEarning')
            ->add('adClickTimeInterval')

        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        $collection->remove('create');

    }
}