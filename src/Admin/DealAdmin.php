<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 4/11/15
 * Time: 5:35 PM
 */

namespace App\Admin;

use App\Model\DealStatus;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DealAdmin extends AbstractAdmin{

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('name', null, array('attr'=>array('readonly'=>'readonly')))
            ->add('coinsToClose')
            ->add('status', ChoiceFieldMaskType::class, array(
                'choices' => array(
                    DealStatus::getAllChoicesForForm()
                )))
            ->add('percentage',null, array('attr'=>array('readonly'=>'readonly')))
            ->add('salt',null, array('attr'=>array('readonly'=>'readonly')))
            ->add('hashKey',null, array('attr'=>array('readonly'=>'readonly')))
            ->add('isFeatured')
            ->add('item',null, array('required'=>true))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            // ->add('name')
            ->add('item.name')
            ->add('item.subCategory.category.name', null, array('label' => 'Category'))
            ->add('status')
            ->add('isFeatured')
//            ->add('user', null, array('label'=>'Winner'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
             ->add('id')
//            ->add('no_tickets_to_close')
//            ->add('status', 'string', array('label' => 'Status','editable' => true, 'template' => 'App:CRUD:list_status_change.html.twig'))
            ->add('item.name')
            ->add('item.subCategory.category.name', null, array('label' => 'Category'))
//            ->add('dealPercentage', null, array('template' => 'App:AdminTemplate:deal_percentage.html.twig'))
            ->add('isFeatured',null, array('editable' => true))
            ->add('user', null, array('label'=>'Winner','route' => array(
                'name' => 'show'
            )))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            // ->add('name')
            ->add('coinsToClose')
            ->add('status')
            ->add('percentage')
            ->add('hashKey')
            ->add('isFeatured')
            ->add('user', null, array('label'=>'Winner'))
//            ->add('dealticket', null,array('template'=>'App:AdminTemplate:deal_show_ticket.html.twig') )

        ;

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');

    }
}