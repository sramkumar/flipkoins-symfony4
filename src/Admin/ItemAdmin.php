<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 4/11/15
 * Time: 3:46 PM
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ItemAdmin extends AbstractAdmin{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();

        $formMapper
            ->with('Item')
                ->add('subCategory')
                ->add('name')
                ->add('description')
                ->add('price')
                ->add('media', ModelListType::class, array('required' => false), array(
                    'link_parameters' => array('context' => 'itemImage')
                ))
                ->add('coverImage', ModelListType::class, array('required' => false), array(
                    'link_parameters' => array('context' => 'coverImage')
                ))

            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('price')
            ->add('subCategory.category.name')
            ->add('subCategory.name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('price')
            ->add('subCategory.category.name')
            ->add('subCategory.name')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'giveaway' => array(
                        'template' => 'admin/list__action_create_deal.html.twig'
                    )
                )
            ))

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('price')
            ->add('subCategory.category.name')
            ->add('subCategory.name')
        ;

    }

    public function prePersist($project)
    {
        $this->preUpdate($project);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        $collection->add('create_deal', $this->getRouterIdParameter().'/deal');

    }
}