<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 2/11/15
 * Time: 12:26 PM
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;

class CategoryAdmin extends AbstractAdmin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $Options = array();
        $formMapper
            ->with('Category')
                ->add('name')
                ->add('description')
                ->add('isEnabled')
                ->add('weight')
                ->add('categoryIcon', ModelListType::class, array('required' => false), array(
                    'link_parameters' => array('context' => 'categoryIcon')
                ))
                ->add('media', CollectionType::class, array(), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'link_parameters' => array(
                        'context' => 'categoryImage',
                        'provider' => 'sonata.media.provider.image'
                    )
                ))

            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('isEnabled')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $Options['help'] = '<img src="" class="admin-preview" />';
        $listMapper
            ->addIdentifier('name')
            ->add('description')
            ->add('isEnabled', null, array('editable' => true))
            ->add('weight')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('description')
            ->add('isEnabled')
            ;

    }

    public function prePersist($category)
    {
        $this->preUpdate($category);
    }

    public function preUpdate($category)
    {
        $category->setCategoryMedias($category->getMedia());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');

    }
}