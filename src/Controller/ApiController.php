<?php
/**
 * Created by PhpStorm.
 * User: ram
 * Date: 23/9/18
 * Time: 11:15 AM
 */

namespace App\Controller;


use App\Response\Collection\DealCollectionResponse;
use App\Response\DealResponse;
use App\Entity\Deal;
use App\Services\CollectCoinService;
use App\Services\DealService;
use App\Services\HeaderGalleryService;
use App\Services\ParticipationService;
use App\Form\Type\ParticipateType;
use App\Form\Type\CollectCoinType;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as FOS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiController extends FOSRestController
{
    /**
     * @var HeaderGalleryService
     */
    private $headerGalleryService;

    /**
     * @var DealService
     */
    private $dealService;

    /**
     * @var ParticipationService
     */
    private $participateService;

    /**
     * @var CollectCoinService
     */
    private $collectCoinService;

    public function __construct(
        HeaderGalleryService $headerGalleryService,
        DealService $dealService,
        ParticipationService $participationService,
        CollectCoinService $collectCoinService
    )
    {
        $this->headerGalleryService = $headerGalleryService;
        $this->dealService = $dealService;
        $this->participateService = $participationService;
        $this->collectCoinService = $collectCoinService;
    }

    /**
     * List the gallery deals.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the gallery deals",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Deal::class, groups={"full"}))
     *     )
     * )
     * @FOS\View(
     *     serializerGroups={"Default"}
     * )
     * @SWG\Tag(name="galleryDeals")
     * @Security(name="Bearer")
     */
    public function getGalleryDealsAction(): DealCollectionResponse
    {
        return $this->headerGalleryService->getHomeGallery();
    }

    /**
     * @param string $slug
     *
     * @Method("GET")
     * @FOS\Route("/deal/{slug}", requirements={"slug"="(.+)[^[.json|.html]"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns single deal data",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Deal::class, groups={"full"}))
     *     )
     * )
     *
     * @FOS\View(
     *     serializerGroups={"Detailed"}
     * )
     *
     * @return DealResponse
     */
    public function getDealAction(string $slug): DealResponse
    {
        try {
            return $this->dealService->getSingleDeal($slug);
        } catch (NoResultException $e) {
            // Throw exception below.
        }

        throw $this->createNotFoundException();
    }

    /**
     *
     * Get all deals.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns all online deals",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Deal::class, groups={"full"}))
     *     )
     * )
     * @FOS\View(
     *     serializerGroups={"Default"}
     * )
     * @SWG\Tag(name="onlineDeals")
     * @Security(name="Bearer")
     */
    public function getDealsAction(): DealCollectionResponse
    {
        return $this->dealService->getEnabledDeals();
    }

    /**
     * @param Request $request
     *
     * @FOS\Route("/participate", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="participate",
     *     in="body",
     *     @Model(type=ParticipateType::class)
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="Participate in a giveaway",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Deal::class, groups={"full"})
     *     )
     * )
     *
     * @Method("POST")
     *
     * @FOS\View(
     *     templateVar="participate",
     *     serializerGroups={"Default"}
     * )
     *
     * @return array
     */
    public function participateAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $userId = $data['user'];
        $dealId = $data['deal'];
        $coins = $data['coins'];
        return $this->participateService->addParticipation($userId, $dealId, $coins);
    }

    /**
     * @param Request $request
     *
     * @FOS\Route("/add-ticket", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="collect-coins",
     *     in="body",
     *     @Model(type=CollectCoinType::class)
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="Collect coins",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Deal::class, groups={"full"})
     *     )
     * )
     *
     * @Method("POST")
     *
     * @FOS\View(
     *     templateVar="collectcoin",
     *     serializerGroups={"Default"}
     * )
     *
     * @return array
     */
    public function addTicketAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $userId = $data['user'];
        return $this->collectCoinService->addTicket($userId);
    }
}