<?php

namespace App\Controller;

use App\Services\CollectCoinService;
use App\Services\DealService;
use App\Services\HeaderGalleryService;
use App\Services\ParticipationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as FOS;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @var HeaderGalleryService
     */
    private $headerGalleryService;

    /**
     * @var DealService
     */
    private $dealService;

    /**
     * @var ParticipationService
     */
    private $participationService;

    /**
     * @var CollectCoinService
     */
    private $collectCoinService;

    public function __construct(
        HeaderGalleryService $headerGalleryService,
        DealService $dealService,
        ParticipationService $participationService,
        CollectCoinService $collectCoinService
    )
    {
        $this->headerGalleryService = $headerGalleryService;
        $this->dealService = $dealService;
        $this->participationService = $participationService;
        $this->collectCoinService = $collectCoinService;
    }

    /**
     * @Route("/", name="homepage")
     *
     */
    public function indexAction(Request $request)
    {
        $galleryDeals = $this->headerGalleryService->getHomeGallery();
        return $this->render('home/index.html.twig', ['galleryDeals' => $galleryDeals]);
    }

    /**
     * @Route("/participatea", name="participatea")
     *
     */
    public function participateAction(Request $request)
    {
        $participation = $this->collectCoinService->addTicket(2);
        dump($participation); exit;
    }
}
