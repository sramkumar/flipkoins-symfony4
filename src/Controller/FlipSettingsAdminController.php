<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 10/11/15
 * Time: 4:59 PM
 */

namespace App\Controller;

use App\Entity\FlipSettings;
use App\Entity\CoinHistory;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class FlipSettingsAdminController extends Controller {

    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $objects = $em->getRepository('App:FlipSettings')->findAll();

        if($objects && $objects[0] instanceof FlipSettings){
            return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $objects[0]->getId())));
        }else{
            throw $this->createNotFoundException('The mandatory settings is empty');
        }
    }

}