<?php
/**
 * Created by PhpStorm.
 * User: hashir
 * Date: 11/11/15
 * Time: 3:41 PM
 */

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ItemAdminController extends Controller{

    public function createDealAction(){

        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id'));
        }

        $env = ($this->container->get( 'kernel' )->getEnvironment() == 'dev') ? 'app_dev.php': 'app.php';

        return new RedirectResponse('/admin/app/deal/create?id='.$object->getId());
    }
}