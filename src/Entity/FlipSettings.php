<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Deal.
 *
 * @ORM\Table(name="flip__settings")
 * @ORM\Entity()
 */
class FlipSettings
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="profitability_value", type="integer")
     */
    private $profitabilityValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="ads_time_intervel", type="integer")
     */
    private $adsTimeInterval;
    /**
     * @var integer
     *
     * @ORM\Column(name="ads_ticket_earning", type="integer")
     */
    private $adsTicketEarning;
    /**
     * @var integer
     *
     * @ORM\Column(name="ad_click_time_interval", type="integer")
     */
    private $adClickTimeInterval;
    /**
     * @var integer
     *
     * @ORM\Column(name="ad_click_ticket_earning", type="integer")
     */
    private $adClickTicketEarning;
    /**
     * @var integer
     *
     * @ORM\Column(name="referral_owner_earning", type="integer")
     */
    private $referralOwnerEarning;
    /**
     * @var integer
     *
     * @ORM\Column(name="referral_earning", type="integer")
     */
    private $referralEarning;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getProfitabilityValue(): int
    {
        return $this->profitabilityValue;
    }

    /**
     * @param int $profitabilityValue
     */
    public function setProfitabilityValue(int $profitabilityValue)
    {
        $this->profitabilityValue = $profitabilityValue;
    }

    /**
     * @return int
     */
    public function getAdsTimeInterval(): int
    {
        return $this->adsTimeInterval;
    }

    /**
     * @param int $adsTimeInterval
     */
    public function setAdsTimeInterval(int $adsTimeInterval)
    {
        $this->adsTimeInterval = $adsTimeInterval;
    }

    /**
     * @return int
     */
    public function getAdsTicketEarning(): int
    {
        return $this->adsTicketEarning;
    }

    /**
     * @param int $adsTicketEarning
     */
    public function setAdsTicketEarning(int $adsTicketEarning)
    {
        $this->adsTicketEarning = $adsTicketEarning;
    }

    /**
     * @return int
     */
    public function getAdClickTimeInterval(): int
    {
        return $this->adClickTimeInterval;
    }

    /**
     * @param int $adClickTimeInterval
     */
    public function setAdClickTimeInterval(int $adClickTimeInterval)
    {
        $this->adClickTimeInterval = $adClickTimeInterval;
    }

    /**
     * @return int
     */
    public function getAdClickTicketEarning(): int
    {
        return $this->adClickTicketEarning;
    }

    /**
     * @param int $adClickTicketEarning
     */
    public function setAdClickTicketEarning(int $adClickTicketEarning)
    {
        $this->adClickTicketEarning = $adClickTicketEarning;
    }

    /**
     * @return int
     */
    public function getReferralOwnerEarning(): int
    {
        return $this->referralOwnerEarning;
    }

    /**
     * @param int $referralOwnerEarning
     */
    public function setReferralOwnerEarning(int $referralOwnerEarning)
    {
        $this->referralOwnerEarning = $referralOwnerEarning;
    }

    /**
     * @return int
     */
    public function getReferralEarning(): int
    {
        return $this->referralEarning;
    }

    /**
     * @param int $referralEarning
     */
    public function setReferralEarning(int $referralEarning)
    {
        $this->referralEarning = $referralEarning;
    }
}
