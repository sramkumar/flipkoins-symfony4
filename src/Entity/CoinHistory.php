<?php

namespace App\Entity;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class CoinHistory.
 *
 * @ORM\Table(name="flip__coin_history")
 * @ORM\Entity(repositoryClass="App\Repository\CoinHistoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class CoinHistory
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_gained", type="integer")
     */
    private $coinGained;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_spent", type="integer")
     */
    private $coinSpent;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_gained", type="integer")
     */
    private $totalGained;

    /**
     * @var integer
     *
     * @ORM\Column(name="total_spent", type="integer")
     */
    private $totalSpent;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks_key", type="string", length=150)
     */
    private $remarksKey;

    /**
     * @var string
     *
     * @ORM\Column(name="remarks", type="string", length=255)
     */
    private $remarks;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_merged", type="boolean", options={"default":"0"})
     */
    private $isMerged = false;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", inversedBy = "coinHistory")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCoinGained(): int
    {
        return $this->coinGained;
    }

    /**
     * @param int $coinGained
     */
    public function setCoinGained(int $coinGained)
    {
        $this->coinGained = $coinGained;
    }

    /**
     * @return int
     */
    public function getCoinSpent(): int
    {
        return $this->coinSpent;
    }

    /**
     * @param int $coinSpent
     */
    public function setCoinSpent(int $coinSpent)
    {
        $this->coinSpent = $coinSpent;
    }

    /**
     * @return int
     */
    public function getTotalGained(): int
    {
        return $this->totalGained;
    }

    /**
     * @param int $totalGained
     */
    public function setTotalGained(int $totalGained)
    {
        $this->totalGained = $totalGained;
    }

    /**
     * @return int
     */
    public function getTotalSpent(): int
    {
        return $this->totalSpent;
    }

    /**
     * @param int $totalSpent
     */
    public function setTotalSpent(int $totalSpent)
    {
        $this->totalSpent = $totalSpent;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock(int $stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getRemarksKey(): string
    {
        return $this->remarksKey;
    }

    /**
     * @param string $remarksKey
     */
    public function setRemarksKey(string $remarksKey)
    {
        $this->remarksKey = $remarksKey;
    }

    /**
     * @return string
     */
    public function getRemarks(): string
    {
        return $this->remarks;
    }

    /**
     * @param string $remarks
     */
    public function setRemarks(string $remarks)
    {
        $this->remarks = $remarks;
    }

    /**
     * @return bool
     */
    public function isMerged(): bool
    {
        return $this->isMerged;
    }

    /**
     * @param bool $isMerged
     */
    public function setIsMerged(bool $isMerged)
    {
        $this->isMerged = $isMerged;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }
}
