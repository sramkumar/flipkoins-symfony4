<?php

namespace App\Entity;

use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class UserSocialAuth.
 *
 * @ORM\Table(name="flip_user_social_auth")
 * @ORM\Entity(repositoryClass="App\Repository\UserSocialAuthRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UserSocialAuth
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_service", type="string", length=255)
     */
    private $oauthService;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_id", type="string", length=255)
     */
    private $oauthId;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_access_token", type="string", length=255, nullable=true)
     */
    private $oauthAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="oauth_secret_token", type="string", length=255, nullable=true)
     */
    private $oauthSecretToken;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", inversedBy = "userSocialAuth")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getOauthService(): string
    {
        return $this->oauthService;
    }

    /**
     * @param string $oauthService
     */
    public function setOauthService(string $oauthService)
    {
        $this->oauthService = $oauthService;
    }

    /**
     * @return string
     */
    public function getOauthId(): string
    {
        return $this->oauthId;
    }

    /**
     * @param string $oauthId
     */
    public function setOauthId(string $oauthId)
    {
        $this->oauthId = $oauthId;
    }

    /**
     * @return string
     */
    public function getOauthAccessToken(): string
    {
        return $this->oauthAccessToken;
    }

    /**
     * @param string $oauthAccessToken
     */
    public function setOauthAccessToken(string $oauthAccessToken)
    {
        $this->oauthAccessToken = $oauthAccessToken;
    }

    /**
     * @return string
     */
    public function getOauthSecretToken(): string
    {
        return $this->oauthSecretToken;
    }

    /**
     * @param string $oauthSecretToken
     */
    public function setOauthSecretToken(string $oauthSecretToken)
    {
        $this->oauthSecretToken = $oauthSecretToken;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }
}
