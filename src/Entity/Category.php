<?php

namespace App\Entity;

use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Category.
 *
 * @ORM\Table(name="flip__category")
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Category
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $isEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;


    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=128)
     */
    private $slug;

    /**
     * @var ArrayCollection|Media[]
     *
     * @ORM\OneToMany(targetEntity="CategoryMedia", mappedBy="category", orphanRemoval=true, cascade={
     *   "persist", "refresh", "remove"  })
     */
    private $media;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="icon_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $categoryIcon;

    /**
     * @var ArrayCollection|SubCategory[]
     *
     * @ORM\OneToMany(targetEntity="SubCategory", mappedBy="category", orphanRemoval=true, cascade={
     *   "persist", "refresh", "remove"  })
     */
    private $subCategory;

    public function __construct()
    {
        $this->isEnabled = false;
        $this->media = new ArrayCollection();
        $this->subCategory = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @param bool $isEnabled
     */
    public function setIsEnabled(bool $isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     */
    public function setWeight(string $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getSlug():string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * Add media
     *
     * @param CategoryMedia $media
     *
     * @return Category
     */
    public function addMedia(CategoryMedia $media)
    {
        $this->media[] = $media;
        $media->setCategory($this);
        return $this;
    }

    /**
     * Remove media
     *
     * @param CategoryMedia $media
     */
    public function removeMedia(CategoryMedia $media)
    {
        $this->media->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return Media
     */
    public function getCategoryIcon()
    {
        return $this->categoryIcon;
    }

    /**
     * @param Media $categoryIcon
     */
    public function setCategoryIcon(Media $categoryIcon)
    {
        $this->categoryIcon = $categoryIcon;
    }


    /**
     * @param ArrayCollection|SubCategory[] $subCategory
     */
    public function setSubCategory(ArrayCollection $subCategory)
    {
        if (count($subCategory) > 0) {
            foreach ($subCategory as $i) {
                $this->addSubCategory($i);
            }
        }
    }

    /**
     * @param SubCategory $subCategory
     */
    public function addSubCategory(SubCategory $subCategory)
    {
        $this->subCategory->add($subCategory);
    }

    /**
     * @return ArrayCollection|SubCategory[]
     */
    public function getSubCategory(): ArrayCollection
    {
        return new ArrayCollection($this->subCategory->toArray());
    }

    public function setCategoryMedias($gameMedias)
    {
        if (count($gameMedias) > 0) {
            foreach ($gameMedias as $i) {
                $this->addMedia($i);
            }
        }

        return $this;
    }

    /**
     * @return string
     * Not working as intended, remove s and try creating new category
     * will throw error that it should return string
     */
    public function __toString()
    {
        return ($this->name)?$this->name:"s";
    }

}
