<?php
/**
 * Created by PhpStorm.
 * User: ram
 * Date: 12/9/17
 * Time: 10:44 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class User.
 *
 * @ORM\Table(name="flip_user_contact")
 * @ORM\Entity(repositoryClass="App\Repository\UserContactRepository")
 */
class UserContact
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login_ip", type="string", length=150)
     */
    private $lastLoginIp;

    /**
     * @var string
     *
     * @ORM\Column(name="last_login_country", type="string", length=150)
     */
    private $lastLoginCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=150)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=150)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=150)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @ORM\OneToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", inversedBy="userContact")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLastLoginIp(): string
    {
        return $this->lastLoginIp;
    }

    /**
     * @param string $lastLoginIp
     */
    public function setLastLoginIp(string $lastLoginIp)
    {
        $this->lastLoginIp = $lastLoginIp;
    }

    /**
     * @return string
     */
    public function getLastLoginCountry(): string
    {
        return $this->lastLoginCountry;
    }

    /**
     * @param string $lastLoginCountry
     */
    public function setLastLoginCountry(string $lastLoginCountry)
    {
        $this->lastLoginCountry = $lastLoginCountry;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}