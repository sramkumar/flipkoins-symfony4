<?php

namespace App\Entity;

use App\Model\DealStatus;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Deal.
 *
 * @ORM\Table(name="flip__deal")
 * @ORM\Entity(repositoryClass="App\Repository\DealRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Deal
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=128)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="coins_to_close", type="integer")
     */
    private $coinsToClose;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=150)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="percentage", type="string", length=255)
     */
    private $percentage;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_key", type="string", length=255)
     */
    private $hashKey;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="winner_drawn_at", type="datetime", nullable=true)
     */
    private $winnerDrawnAt;

    /**
     * @var string
     *
     * @ORM\Column(name="address_confirmation_token", type="string", length=255, nullable=true)
     */
    private $addressConfirmationToken;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_featured", type="boolean")
     */
    private $isFeatured;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $user;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Item", inversedBy = "deal")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="item_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $item;

    /**
     * @var ArrayCollection|DealCoin[]
     *
     * @ORM\OneToMany(targetEntity="DealCoin", mappedBy="deal", orphanRemoval=true, cascade={
     *   "persist", "refresh", "remove"  })
     */
    private $dealCoin;

    /**
     * @param string $status
     */
    public function __construct($status = DealStatus::DRAFT) {
        $this->status = $status;
        $this->dealCoin = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getCoinsToClose()
    {
        return $this->coinsToClose;
    }

    /**
     * @param int $coinsToClose
     */
    public function setCoinsToClose(int $coinsToClose)
    {
        $this->coinsToClose = $coinsToClose;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt(string $salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param string $percentage
     */
    public function setPercentage(string $percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * @return string
     */
    public function getHashKey()
    {
        return $this->hashKey;
    }

    /**
     * @param string $hashKey
     */
    public function setHashKey(string $hashKey)
    {
        $this->hashKey = $hashKey;
    }

    /**
     * @return \DateTime
     */
    public function getWinnerDrawnAt()
    {
        return $this->winnerDrawnAt;
    }

    /**
     * @param \DateTime $winnerDrawnAt
     */
    public function setWinnerDrawnAt(\DateTime $winnerDrawnAt)
    {
        $this->winnerDrawnAt = $winnerDrawnAt;
    }

    /**
     * @return string
     */
    public function getAddressConfirmationToken()
    {
        return $this->addressConfirmationToken;
    }

    /**
     * @param string $addressConfirmationToken
     */
    public function setAddressConfirmationToken(string $addressConfirmationToken)
    {
        $this->addressConfirmationToken = $addressConfirmationToken;
    }

    /**
     * @return bool
     */
    public function isFeatured()
    {
        return $this->isFeatured;
    }

    /**
     * @param bool $isFeatured
     */
    public function setIsFeatured(bool $isFeatured)
    {
        $this->isFeatured = $isFeatured;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item)
    {
        $this->item = $item;
    }

    /**
     * @param ArrayCollection|DealCoin[] $dealCoin
     */
    public function setDealCoin(ArrayCollection $dealCoin)
    {
        $this->dealCoin = new ArrayCollection();

        foreach ($dealCoin as $coins) {
            $this->addDealCoin($coins);
        }
    }

    /**
     * @param DealCoin $dealCoin
     */
    public function addDealCoin(DealCoin $dealCoin)
    {
        $this->dealCoin->add($dealCoin);
    }

    /**
     * @return ArrayCollection|DealCoin[]
     */
    public function getDealCoin(): ArrayCollection
    {
        return new ArrayCollection($this->dealCoin->toArray());
    }
}
