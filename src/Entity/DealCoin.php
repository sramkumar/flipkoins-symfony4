<?php

namespace App\Entity;

use App\Model\DealStatus;
use App\Application\Sonata\MediaBundle\Entity\Media;
use App\Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Deal.
 *
 * @ORM\Table(name="flip__deal_coins")
 * @ORM\Entity(repositoryClass="App\Repository\DealCoinRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class DealCoin
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_spent", type="integer")
     */
    private $coinSpent;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_from", type="integer")
     */
    private $coinFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="coin_to", type="integer")
     */
    private $coinTo;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\UserBundle\Entity\User", inversedBy = "dealCoin")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $user;

    /**
     * @var Deal
     *
     * @ORM\ManyToOne(targetEntity="Deal", inversedBy = "dealCoin")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="deal_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $deal;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCoinSpent(): int
    {
        return $this->coinSpent;
    }

    /**
     * @param int $coinSpent
     */
    public function setCoinSpent(int $coinSpent)
    {
        $this->coinSpent = $coinSpent;
    }

    /**
     * @return int
     */
    public function getCoinFrom(): int
    {
        return $this->coinFrom;
    }

    /**
     * @param int $coinFrom
     */
    public function setCoinFrom(int $coinFrom)
    {
        $this->coinFrom = $coinFrom;
    }

    /**
     * @return int
     */
    public function getCoinTo(): int
    {
        return $this->coinTo;
    }

    /**
     * @param int $coinTo
     */
    public function setCoinTo(int $coinTo)
    {
        $this->coinTo = $coinTo;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @param Deal $deal
     */
    public function setDeal(Deal $deal)
    {
        $this->deal = $deal;
    }
}
