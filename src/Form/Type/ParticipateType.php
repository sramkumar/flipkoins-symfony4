<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ParticipateType.
 */
class ParticipateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'user',
                TextType::class,
                [
                    'documentation' => [
                        'type' => 'string', // would have been automatically detected in this case
                        'description' => 'User Id',
                    ],
                ]
            )
            ->add(
                'deal',
                TextType::class,
                [
                    'documentation' => [
                        'type' => 'string', // would have been automatically detected in this case
                        'description' => 'Deal Id',
                    ],
                ]
            )
            ->add(
                'coins',
                TextType::class,
                [
                    'documentation' => [
                        'type' => 'string', // would have been automatically detected in this case
                        'description' => 'Coins to participate',
                    ],
                ]
            )
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
