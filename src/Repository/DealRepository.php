<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Deal;
use App\Model\DealStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class DealRepository.
 */
class DealRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Deal::class);
    }

    /**
     * @param Category|null $category
     * @return ArrayCollection
     */
    public function findDealsIsFeatured($category = null): ArrayCollection
    {
        $sql = 'SELECT d FROM App:Deal d
        INNER JOIN d.item it
        INNER JOIN it.subCategory sc
        INNER JOIN sc.category c
        WHERE c.isEnabled = :enabled
        AND d.isFeatured = :featured
        AND d.status IN(:status)
        ORDER BY c.weight DESC';

        $parameters = [
            'enabled' => true,
            'featured' => true,
            'status' => DealStatus::getApplicableStatusForGallery(),
        ];

        return new ArrayCollection(
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->getResult()
        );
    }

    /**
     * @return ArrayCollection
     */
    public function findEnabledDeals()
    {
        $sql = 'SELECT d FROM App:Deal d
        INNER JOIN d.item it
        INNER JOIN it.subCategory sc
        INNER JOIN sc.category c
        WHERE c.isEnabled = :enabled
        AND d.status IN(:status)
        ORDER BY c.weight DESC';

        $parameters = [
            'enabled' => true,
            'status' => DealStatus::getOnlyOnlineDeals(),
        ];

        return new ArrayCollection(
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->getResult()
        );
    }

    /**
     * @param $slug
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSingleDeal($slug)
    {
        $sql = 'SELECT d FROM App:Deal d
        INNER JOIN d.item it
        INNER JOIN it.subCategory sc
        INNER JOIN sc.category c
        WHERE c.isEnabled = :enabled
        AND d.status != :status
        AND d.slug = :slug
        ORDER BY c.weight DESC';

        $parameters = [
            'enabled' => true,
            'slug' => $slug,
            'status' => DealStatus::DRAFT,
        ];

        return
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->getSingleResult();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDealById($id)
    {
        $sql = 'SELECT d FROM App:Deal d
        INNER JOIN d.item it
        INNER JOIN it.subCategory sc
        INNER JOIN sc.category c
        WHERE c.isEnabled = :enabled
        AND d.status != :status
        AND d.id = :id
        ORDER BY c.weight DESC';

        $parameters = [
            'enabled' => true,
            'id' => $id,
            'status' => DealStatus::DRAFT,
        ];

        return
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->getSingleResult();
    }

    /**
     * @param Deal $deal
     * @throws
     */
    public function persist(Deal $deal)
    {
        $this->getEntityManager()->persist($deal);
    }

    /**
     * @param Deal $deal
     * @throws
     */
    public function remove(Deal $deal)
    {
        $this->getEntityManager()->remove($deal);
    }
}
