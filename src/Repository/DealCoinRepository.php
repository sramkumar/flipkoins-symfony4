<?php

namespace App\Repository;

use App\Entity\Deal;
use App\Entity\DealCoin;
use App\Model\DealStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class DealCoinRepository.
 */
class DealCoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Deal::class);
    }

    /**
     * @param DealCoin $dealCoin
     * @throws
     */
    public function persist(DealCoin $dealCoin)
    {
        $this->getEntityManager()->persist($dealCoin);
    }

    /**
     * @param DealCoin $dealCoin
     * @throws
     */
    public function remove(DealCoin $dealCoin)
    {
        $this->getEntityManager()->remove($dealCoin);
    }

    /**
     * @param $dealId
     * @return mixed
     */
    public function getDealPercentage($dealId)
    {
        $sql = 'SELECT d FROM App:DealCoin d
        WHERE d.deal = :dealId
        ORDER BY d.id DESC';

        $parameters = [
            'dealId' => $dealId,
        ];

        $qb =
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->setFirstResult(0)
                ->setMaxResults(1)
                ->execute();

        if(count($qb) > 0 ){
            return $qb[0];
        }

        return $qb;
    }

    /**
     * @param $dealId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastParticipation($dealId)
    {
        $sql = 'SELECT d FROM App:DealCoin d
        WHERE d.deal = :dealId
        ORDER BY d.coinTo DESC';

        $parameters = [
            'dealId' => $dealId,
        ];

        $qb=
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->setFirstResult(0)
                ->setMaxResults(1);

        $result = $qb->useResultCache(false)->getOneOrNullResult();

        return $result;
    }
}
