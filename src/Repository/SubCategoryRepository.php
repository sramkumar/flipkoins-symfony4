<?php

namespace App\Repository;

use App\Entity\SubCategory;
use Doctrine\ORM\EntityRepository;

/**
 * Class SubCategoryRepository.
 */
class SubCategoryRepository extends EntityRepository
{

    /**
     * @param SubCategory $subCategory
     */
    public function persist(SubCategory $subCategory)
    {
        $this->getEntityManager()->persist($subCategory);
    }

    /**
     * @param SubCategory $subCategory
     */
    public function remove(SubCategory $subCategory)
    {
        $this->getEntityManager()->remove($subCategory);
    }
}
