<?php

namespace App\Repository;

use App\Entity\UserSocialAuth;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserSocialAuthRepository.
 */
class UserSocialAuthRepository extends EntityRepository
{

    /**
     * @param UserSocialAuth $userSocialAuth
     */
    public function persist(UserSocialAuth $userSocialAuth)
    {
        $this->getEntityManager()->persist($userSocialAuth);
    }

    /**
     * @param UserSocialAuth $userSocialAuth
     */
    public function remove(UserSocialAuth $userSocialAuth)
    {
        $this->getEntityManager()->remove($userSocialAuth);
    }
}
