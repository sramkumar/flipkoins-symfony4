<?php

namespace App\Repository;

use App\Entity\CoinHistory;
use Doctrine\ORM\EntityRepository;

/**
 * Class CoinHistoryRepository
 */
class CoinHistoryRepository extends EntityRepository
{

    /**
     * @param CoinHistory $coinHistory
     * @throws
     */
    public function persist(CoinHistory $coinHistory)
    {
        $this->getEntityManager()->persist($coinHistory);
    }

    /**
     * @param CoinHistory $coinHistory
     * @throws
     */
    public function remove(CoinHistory $coinHistory)
    {
        $this->getEntityManager()->remove($coinHistory);
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLatest($userId)
    {
        $sql = 'SELECT d FROM App:CoinHistory d
        WHERE d.user = :userId
        ORDER BY d.updatedAt DESC';

        $parameters = [
            'userId' => $userId,
        ];

        $qb=
            $this->getEntityManager()
                ->createQuery($sql)
                ->setParameters($parameters)
                ->setCacheable(true)
                ->setFirstResult(0)
                ->setMaxResults(1);

        $result = $qb->useResultCache(false)->getOneOrNullResult();

        return $result;
    }
}
