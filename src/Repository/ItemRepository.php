<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\ORM\EntityRepository;

/**
 * Class ItemRepository.
 */
class ItemRepository extends EntityRepository
{

    /**
     * @param Item $item
     */
    public function persist(Item $item)
    {
        $this->getEntityManager()->persist($item);
    }

    /**
     * @param Item $item
     */
    public function remove(Item $item)
    {
        $this->getEntityManager()->remove($item);
    }
}
