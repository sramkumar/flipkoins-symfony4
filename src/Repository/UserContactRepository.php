<?php

namespace App\Repository;

use App\Entity\UserContact;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserContactRepository.
 */
class UserContactRepository extends EntityRepository
{

    /**
     * @param UserContact $userContact
     */
    public function persist(UserContact $userContact)
    {
        $this->getEntityManager()->persist($userContact);
    }

    /**
     * @param UserContact $userContact
     */
    public function remove(UserContact $userContact)
    {
        $this->getEntityManager()->remove($userContact);
    }
}
