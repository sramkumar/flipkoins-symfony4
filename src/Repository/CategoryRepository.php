<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends EntityRepository
{

    /**
     * @param Category $category
     */
    public function persist(Category $category)
    {
        $this->getEntityManager()->persist($category);
    }

    /**
     * @param Category $category
     */
    public function remove(Category $category)
    {
        $this->getEntityManager()->remove($category);
    }
}
