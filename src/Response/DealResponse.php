<?php

namespace App\Response;

use App\Entity\Deal;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Annotation as JMS;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Routing\Router;

/**
 * Class DealResponse.
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class DealResponse
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->deal->getName();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getHashKey(): string
    {
        return $this->deal->getHashKey();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getPercentage(): string
    {
        return $this->deal->getPercentage();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getSlug(): string
    {
        return $this->deal->getSlug();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->deal->getStatus();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return \DateTime
     */
    public function getWinnerDrawnAt()
    {
        return $this->deal->getWinnerDrawnAt();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getCoinsToClose()
    {
        return $this->deal->getCoinsToClose();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("DateTime")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->deal->getUpdatedAt();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return \DateTime
     */
    public function getItemImage()
    {
        return $this->generateMediaPublicUrl($this->deal->getItem()->getMedia());
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Default","Detailed"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getCoverImage()
    {
        return $this->generateMediaPublicUrl($this->deal->getItem()->getCoverImage());
    }

    /**
     * @param $media
     * @return null|string
     */
    private function generateMediaPublicUrl($media)
    {
        $container = $GLOBALS['kernel']->getContainer();
        if($media) {
            /** @var MediaProviderInterface $provider */
            $provider = $container->get($media->getProviderName());
            /** @var Router $router */
            $router = $container->get('router');

            if($media->getProviderName() == 'sonata.media.provider.youtube'){
                $providerMetadata = $media->getProviderMetadata();
                return $providerMetadata['provider_url'].'watch?v='.$media->getProviderReference();
            }

            $webUrl = $router->getContext()->getScheme().':/'. $host = $router->getContext()->getHost();
            return $webUrl . $provider->generatePublicUrl($media, 'reference');
        }
        return null;
    }
}
