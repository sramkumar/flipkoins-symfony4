<?php

namespace App\Response\Collection;

use App\Entity\Deal;
use App\Response\DealResponse;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class DealCollectionResponse.
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class DealCollectionResponse
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var ArrayCollection|DealResponse[]
     */
    private $deals;

    /**
     * DealCollectionResponse constructor.
     */
    public function __construct()
    {
        $this->deals = new ArrayCollection();
        $this->count = 0;
    }

    /**
     * @param Collection|Deal[] $deals
     *
     * @return DealCollectionResponse
     */
    public static function createFromResult(Collection $deals)
    {
        $instance = new self();

        foreach ($deals as $deal) {
            $instance->deals->add(new DealResponse($deal));
        }

        $instance->count = $instance->deals->count();

        return $instance;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("integer")
     * @JMS\Groups({"Notification"})
     * @JMS\Since("1.0")
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("ArrayCollection<App\Response\DealResponse>")
     * @JMS\Groups({"Default"})
     * @JMS\Since("1.0")
     *
     * @return ArrayCollection
     */
    public function getDeals(): ArrayCollection
    {
        return $this->deals;
    }
}
