<?php
/**
 * Created by PhpStorm.
 * User: ram
 * Date: 4/10/18
 * Time: 10:40 PM
 */

namespace App\Services;

use App\Model\DealStatus;
use App\Repository\DealCoinRepository;
use App\Repository\DealRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ParticipationService
 * @package App\Services
 */
class ParticipationService
{
    /**
     * @var DealRepository
     */
    private $dealRepository;

    /**
     * @var DealCoinService
     */
    private $dealCoinService;

    /**
     * @var DealCoinRepository
     */
    private $dealCoinRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ParticipationService constructor.
     * @param DealRepository $dealRepository
     * @param DealCoinService $dealCoinService
     * @param EntityManagerInterface $em
     * @param DealCoinRepository $dealCoinRepository
     */
    public function __construct(
        DealRepository $dealRepository,
        DealCoinService $dealCoinService,
        EntityManagerInterface $em,
        DealCoinRepository $dealCoinRepository
    )
    {
        $this->dealRepository = $dealRepository;
        $this->dealCoinService = $dealCoinService;
        $this->em = $em;
        $this->dealCoinRepository = $dealCoinRepository;
    }

    /**
     * @param $userId
     * @param $dealId
     * @param $coins
     * @return array
     * @throws
     */
    public function addParticipation($userId, $dealId, $coins)
    {
        $dealObj = $this->dealRepository->getDealById($dealId);
        $coinsToClose = $dealObj->getCoinsToClose();

        $dealPercentage = $this->dealCoinService->getDealPercentage($dealId, $coinsToClose);

        /**
         * to check is participation closed
         * and allow spending while correcting the winner user (to chose instead of banned user)
         */
        if($dealPercentage['coinSpent']>= $coinsToClose){
            return array('msg' => 'Giveaway participation closed.', 'percentage' => $dealPercentage['percentage']);
        }

        /**
         * check the participant feasibility
         */
        if (!$this->participateFeasibility($userId, $dealId, $coins)) {
            return array('msg' => 'You don’t have enough coins or something wrong happened, try again later.', 'percentage' => $dealPercentage['percentage']);
        }

        $lastParticipation = $this->dealCoinRepository->findLastParticipation($dealId);

        $coinsSpend = $coins;
        $coinsFrom = 1;
        $coinsTo = $coins;
        if ($lastParticipation){
            $coinsFrom += $lastParticipation->getCoinTo();
            $coinsTo += $lastParticipation->getCoinTo();
        }

        $participation = $this->em->getConnection()
            ->prepare('INSERT INTO `flip__deal_coins`(`deal_id`, `user_id`, `coin_spent`, `coin_from`, `coin_to`, `created_at`, `updated_at`) VALUES (?,?,?,?,?,?,?)');
        $participation->bindValue(1, $dealId);
        $participation->bindValue(2, $userId);
        $participation->bindValue(3, $coinsSpend);
        $participation->bindValue(4, $coinsFrom);
        $participation->bindValue(5, $coinsTo);
        $participation->bindValue(6, date('Y-m-d H:i:s'));
        $participation->bindValue(7, date('Y-m-d H:i:s'));

        try {
            $participation->execute();
            $this->em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return array('msg' => 'You are in queue, a player is spending coins right now, try again later.', 'percentage' => $dealPercentage['percentage']);
        }

        $coinHistoryQuery = $this->em->getConnection()
            ->prepare('SELECT id,user_id, coin_gained, coin_spent, total_gained, total_spent, stock, updated_at, remarks FROM `flip__coin_history` where deleted_at IS NULL AND user_id = :userId order by updated_at DESC limit 1');
        $coinHistoryQuery->bindValue(':userId', $userId);
        $coinHistoryQuery->execute();
        $coinHistory = $coinHistoryQuery->fetchAll();

        /**
         * Ticket History can't be null
         */
        $coinHistory = $coinHistory[0];
        $total_gained = $coinHistory['total_gained'];
        $stock = $coinHistory['stock'] - $coins;
        $total_spent = $coinHistory['total_spent'] +$coins;

        $dealObjFromId = $this->dealRepository->getDealById($dealId);
        $dealRemarks = $dealObjFromId->getName();
        $dealRemarksKey = $dealObjFromId->getSlug();

        $dealTicket = $this->em->getConnection()
            ->prepare('INSERT INTO `flip__coin_history`(`user_id`, `coin_gained`, `total_gained`, `stock`, `remarks_key`, `remarks`, `coin_spent`, `total_spent`, `created_at`,`updated_at`) VALUES (?,?,?,?,?, ?, ?, ?, ?, ?)');
        $dealTicket->bindValue(1, $userId);
        $dealTicket->bindValue(2, 0);
        $dealTicket->bindValue(3, $total_gained);
        $dealTicket->bindValue(4, $stock);
        $dealTicket->bindValue(5, $dealRemarks);
        $dealTicket->bindValue(6, $dealRemarksKey);
        $dealTicket->bindValue(7, $coins);
        $dealTicket->bindValue(8, $total_spent);
        $dealTicket->bindValue(9, date('Y-m-d H:i:s'));
        $dealTicket->bindValue(10, date('Y-m-d H:i:s'));
        $dealTicket->execute();

        $this->em->flush();
        /**
         * check the giveaway percentage, if 100, winner drawing
         * and no action while correcting the winner user (to chose instead of banned user)
         */
        $newDealPercentage = floor(($dealPercentage['coinSpent'] + $coins)/$coinsToClose*100);
        $newDealPercentage = $newDealPercentage > 100 ? 100: $newDealPercentage;

        if($newDealPercentage >= 100){
            $totalCoinsSpent = $dealPercentage['coinSpent'] + $coins;
            $this->winnerDrawing($dealObj, $totalCoinsSpent);
            return array('msg' =>"Winner drawn..!", 'percentage'=>100);
        }


        return array('msg' =>"You have successfully spent $coins coins on $dealRemarks",'deal'=> $dealId, 'percentage'=>$newDealPercentage);
    }

    /**
     * @param $userId
     * @param $dealId
     * @param $coins
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function participateFeasibility($userId, $dealId, $coins)
    {
        /**
         * checks is Giveaway exists
         */
        $dealQuery = $this->em->getConnection()
            ->prepare('SELECT id,item_id, status FROM `flip__deal` where deleted_at IS NULL AND status =:stat AND id = :dealId');
        $dealQuery->bindValue(':stat', 'online');
        $dealQuery->bindValue(':dealId', $dealId);
        $dealQuery->execute();
        $dealObj = $dealQuery->fetchAll();

        if (!$dealObj) {
            return false;
        }

        /**
         * checks is enough tickets earned
         */
        $coinHistoryQuery = $this->em->getConnection()
            ->prepare('SELECT stock FROM `flip__coin_history` where deleted_at IS NULL AND user_id = :userId order by updated_at DESC limit 1');
        $coinHistoryQuery->bindValue(':userId', $userId);
        $coinHistoryQuery->execute();
        $coinHistory = $coinHistoryQuery->fetchAll();
        if (!$coinHistory || $coinHistory[0]['stock'] < $coins){
            return false;
        }

        return true;
    }

    /**
     * @param $dealObj
     * @param $totalCoinSpent
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function winnerDrawing($dealObj, $totalCoinSpent)
    {
        $numberOfCoins = $totalCoinSpent;
        $winnerPercentage = $dealObj->getPercentage();

        $profitabilityValue = 0.0000000001;

        $dealCoinsTo = floor( ( $numberOfCoins - $profitabilityValue ) * ( ($winnerPercentage / 100) ) );

        $query = $this->em
            ->createQueryBuilder('g')
            ->select('gt')
            ->from('App:DealCoin', 'gt')
            ->leftJoin('gt.deal', 'gi')
            ->where('gi.id = :gid')
            ->andWhere('gt.coinTo >= :coinsTo')
            ->andWhere('gt.coinFrom <= :coinsTo')
            ->setParameter(':gid', $dealObj->getId())
            ->setParameter(':coinsTo', $dealCoinsTo)
            ->orderBy('gt.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getSingleResult();

        $user = $query->getUser();


        $dealObj->setStatus(DealStatus::WINNER_DRAWN);

        $dealObj->setUser($user);
        $dealObj->setIsFeatured(false);
        $dealObj->setWinnerDrawnAt(new \DateTime());


        $this->em->persist($dealObj);
        $this->em->flush();

//        $dealMailer = $this->container->get('app.deal_mailer');
//        $dealMailer->sendAlertMail($dealObj);

    }
}