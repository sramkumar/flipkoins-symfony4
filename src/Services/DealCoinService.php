<?php

namespace App\Services;

use App\Repository\DealCoinRepository;

/**
 * Class DealCoinService.
 *
 */
class DealCoinService
{
    /**
     * @var DealCoinRepository
     */
    private $dealCoinRepository;

    /**
     * @param DealCoinRepository $dealCoinRepository
     */
    public function __construct(DealCoinRepository $dealCoinRepository)
    {
        $this->dealCoinRepository = $dealCoinRepository;
    }

    /**
     * @param $dealId
     * @param $coinsToClose
     * @return array
     * @throws
     */
    public function getDealPercentage($dealId, $coinsToClose)
    {
        $coins = $this->dealCoinRepository->getDealPercentage($dealId);
        /**
         * Checks if coin exists
         */
        $coinSpent = 0;
        if ($coins)
            $coinSpent = $coins->getCoinTo();

        $result = floor((int)$coinSpent/$coinsToClose*100);
        return array(
            'percentage' => $result > 100 ? 100: $result,
            'coinSpent' => $coinSpent
        );
    }
}
