<?php

namespace App\Services;

use App\Repository\DealRepository;
use App\Response\Collection\DealCollectionResponse;
use App\Response\DealResponse;

/**
 * Class DealService.
 *
 */
class DealService
{
    /**
     * @var DealRepository
     */
    private $dealRepository;

    /**
     * @param DealRepository $dealRepository
     */
    public function __construct(DealRepository $dealRepository)
    {
        $this->dealRepository = $dealRepository;
    }

    /**
     * @param $slug
     * @return DealResponse
     */
    public function getSingleDeal($slug): DealResponse
    {
        $deal = $this->dealRepository->getSingleDeal($slug);
        return new DealResponse($deal);
    }

    /**
     * @return DealCollectionResponse
     */
    public function getEnabledDeals(): DealCollectionResponse
    {
        $deals = $this->dealRepository->findEnabledDeals();
        return DealCollectionResponse::createFromResult($deals);
    }
}
