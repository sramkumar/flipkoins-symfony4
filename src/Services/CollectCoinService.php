<?php
/**
 * Created by PhpStorm.
 * User: ram
 * Date: 4/10/18
 * Time: 10:40 PM
 */

namespace App\Services;

use App\Entity\CoinHistory;
use App\Model\DealStatus;
use App\Repository\DealCoinRepository;
use App\Repository\DealRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CollectCoinService
 * @package App\Services
 */
class CollectCoinService
{
    /**
     * @var DealRepository
     */
    private $dealRepository;

    /**
     * @var DealCoinService
     */
    private $dealCoinService;

    /**
     * @var DealCoinRepository
     */
    private $dealCoinRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ParticipationService constructor.
     * @param DealRepository $dealRepository
     * @param DealCoinService $dealCoinService
     * @param EntityManagerInterface $em
     * @param DealCoinRepository $dealCoinRepository
     */
    public function __construct(
        DealRepository $dealRepository,
        DealCoinService $dealCoinService,
        EntityManagerInterface $em,
        DealCoinRepository $dealCoinRepository
    )
    {
        $this->dealRepository = $dealRepository;
        $this->dealCoinService = $dealCoinService;
        $this->em = $em;
        $this->dealCoinRepository = $dealCoinRepository;
    }

    public function updateTicketHistory($userId, $coinsCount, $actionRequired=null, $type=null)
    {
        $userObj = $this->em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('id' => $userId));

        if($userObj) {
            $coinHistoryObj = $this->em->getRepository('App:CoinHistory')->findLatest($userId);

            if ($coinHistoryObj) {
                $coinHistory = clone $coinHistoryObj;
                if ($actionRequired == 'add') {
                    $this->updateGainedTicket($coinHistoryObj, $coinsCount, $coinHistory, $userObj, $type);
                } else {
                    $this->updateSpentTicket($coinHistoryObj, $coinsCount, $coinHistory, $userObj, $type);
                }
            }else{
                $coinHistory = new CoinHistory();
                if ($actionRequired == 'add') {
                    $coinHistory->setCoinSpent(0);
                    $coinHistory->setTotalSpent(0);
                    $coinHistory->setCoinGained($coinsCount);
                    $coinHistory->setTotalGained($coinsCount);
                    $coinHistory->setStock($coinsCount);
                } else {
                    $response['fail'] = "No Ticket history found for the user";
                    return $response;
                }
                $coinHistory->setUser($userObj);
                $coinHistory->setRemarks($type);
                $this->em->persist($coinHistory);
                $this->em->flush();
            }
            $response['result'] = 'success';
        }else{
            $response['result'] = 'failure';
        }
        return $response;
    }

    public function updateGainedTicket($coinHistoryObj, $coinsCount, CoinHistory $coinHistory, $userObj, $type)
    {
        $totalGained = $coinHistoryObj->getTotalGained();
        $totalGained = ($totalGained + $coinsCount);
        $coinHistory->setCoinSpent(0);
        $coinHistory->setCreatedAt(new \DateTime('now'));
        $coinHistory->setUpdatedAt(new \DateTime('now'));
        $coinHistory->setTotalGained($totalGained);
        $coinHistory->setCoinGained($coinsCount);
        $stockValue = ($coinHistoryObj->getStock() + $coinsCount);
        $coinHistory->setStock($this->roundOfNegative($stockValue));
        $coinHistory->setRemarks('Admin Added');
        $coinHistory->setRemarksKey($type);
        $coinHistory->setUser($userObj);
        $this->em->persist($coinHistory);
        $this->em->flush();
        return true;

    }

    public function updateSpentTicket($coinHistoryObj, $ticketCount, $coinHistory, $userObj,$type)
    {
        $totalSpent = $coinHistoryObj->getTotalSpent();
        $totalSpent = ($totalSpent + $ticketCount);
        $coinHistory->setCoinGained(0);
        $coinHistory->setCreatedAt(new \DateTime('now'));
        $coinHistory->setUpdatedAt(new \DateTime('now'));
        $coinHistory->setTotalSpent($totalSpent);
        $coinHistory->setCoinSpent($ticketCount);
        $stockValue = ($coinHistoryObj->getStock()-$ticketCount);
        $coinHistory->setStock($this->roundOfNegative($stockValue));
        $coinHistory->setRemarks('Admin Removed');
        $coinHistory->setRemarksKey($type);
        $coinHistory->setUser($userObj);
        $this->em->persist($coinHistory);
        $this->em->flush();
        return true;
    }


    public function roundOfNegative($value){
        return ($value < 0 ? 0 : $value);
    }


    public function addTicket($userId)
    {
        $settingsQuery = $this->em->getConnection()
            ->prepare('select ads_time_intervel, ads_ticket_earning from flip__settings');
        $settingsQuery->execute();
        $settings = $settingsQuery->fetchAll();

        /**
         * Checks is Settings exists
         */
        if (count($settings))
            $settings = $settings[0];
        else
            return array('msg' => 'Error code: Gravity'); // Gravity = huge server error

        $coinHistoryQuery = $this->em->getConnection()
            ->prepare('SELECT id,user_id, coin_gained, coin_spent, total_gained, total_spent, stock, updated_at, remarks_key FROM `flip__coin_history` where deleted_at IS NULL AND user_id = :userId order by updated_at DESC limit 1');
        $coinHistoryQuery->bindValue(':userId', $userId);
        $coinHistoryQuery->execute();
        $coinHistory = $coinHistoryQuery->fetchAll();
        if (count($coinHistory))
            $coinHistory = $coinHistory[0];
        else
        {
            /**
             * if ticket history null, no records yet, direct insert
             */
            $ticket = $this->em->getConnection()
                ->prepare('INSERT INTO `flip__coin_history`(`user_id`, `coin_gained`, `total_gained`, `stock`, `remarks_key`, `remarks`, `coin_spent`, `total_spent`, `created_at`,`updated_at`) VALUES (?,?,?,?,"Adv", "Stream Watching", ?, ?, ?, ?)');
            $ticket->bindValue(1, $userId);
            $ticket->bindValue(2, $settings['ads_ticket_earning']);
            $ticket->bindValue(3, $settings['ads_ticket_earning']);
            $ticket->bindValue(4, $settings['ads_ticket_earning']);
            $ticket->bindValue(5, 0);
            $ticket->bindValue(6, 0);
            $ticket->bindValue(7, date('Y-m-d H:i:s'));
            $ticket->bindValue(8, date('Y-m-d H:i:s'));
            $ticket->execute();
            return array('msg' => 'Added', 'ticket'=>$settings['ads_ticket_earning']);
        }




        $updated_time = strtotime($coinHistory['updated_at']);
        $current_time = strtotime(date('Y-m-d H:i:s'));
        $difference = abs($current_time - $updated_time);
        $interval = $settings['ads_time_intervel'];

        /**
         * Checks is Request is Valid, by matching last updated time in DB.
         * Merge the row if the last inserted row is "Adv" and same day
         * else create new record
         */
        if($coinHistory['remarks_key'] === "Adv" &&
            $difference < $interval*0.9){
            return array('msg'=> 'Error Code: Baboon'); // Baboon = difference < interval from ticket history
        }elseif($coinHistory['remarks_key'] === "Adv" && date('Y-m-d', $updated_time) === date('Y-m-d', $current_time)){
            $ticket = $this->em->getConnection()
                ->prepare('UPDATE `flip__coin_history` SET `coin_gained`= :gained,`total_gained`= :totalGained,`stock`= :stock,`remarks_key`= "Adv",`updated_at`= :updated WHERE id= :id');
            $ticket->bindValue(':gained', $settings['ads_ticket_earning']+$coinHistory['coin_gained']);
            $ticket->bindValue(':totalGained', $settings['ads_ticket_earning']+$coinHistory['total_gained']);
            $ticket->bindValue(':stock', $settings['ads_ticket_earning']+$coinHistory['stock']);
            $ticket->bindValue(':updated', date('Y-m-d H:i:s'));
            $ticket->bindValue(':id', $coinHistory['id']);
            $ticket->execute();
            return array('msg' => 'Updated', 'ticket'=>$settings['ads_ticket_earning']);
        }else{

            $ticket = $this->em->getConnection()
                ->prepare('INSERT INTO `flip__coin_history`(`user_id`, `coin_gained`, `total_gained`, `stock`, `remarks_key`,`remarks`, `coin_spent`, `total_spent`, `created_at`,`updated_at`) VALUES (?,?,?,?,"Adv","Stream Watching", ?, ?, ?, ?)');
            $ticket->bindValue(1, $userId);
            $ticket->bindValue(2, $settings['ads_ticket_earning']);
            $ticket->bindValue(3, $settings['ads_ticket_earning']+$coinHistory['total_gained']);
            $ticket->bindValue(4, $settings['ads_ticket_earning']+$coinHistory['stock']);
            $ticket->bindValue(5, 0);
            $ticket->bindValue(6, $coinHistory['total_spent']);
            $ticket->bindValue(7, date('Y-m-d H:i:s'));
            $ticket->bindValue(8, date('Y-m-d H:i:s'));
            $ticket->execute();
            return array('msg' => 'Added', 'ticket'=>$settings['ads_ticket_earning']);
        }
    }

}