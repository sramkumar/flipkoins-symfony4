<?php

namespace App\Services;

use App\Entity\Category;
use App\Repository\DealRepository;
use App\Response\Collection\DealCollectionResponse;

/**
 * Class HeaderGalleryService.
 *
 */
class HeaderGalleryService
{
    /**
     * @var DealRepository
     */
    private $dealRepository;

    /**
     * @param DealRepository $dealRepository
     */
    public function __construct(DealRepository $dealRepository)
    {
        $this->dealRepository = $dealRepository;
    }

    /**
     * @param null $category
     * @return DealCollectionResponse
     */
    public function getHomeGallery($category=null): DealCollectionResponse
    {
        $deals = $this->dealRepository->findDealsIsFeatured($category);
        return DealCollectionResponse::createFromResult($deals);
    }
}
