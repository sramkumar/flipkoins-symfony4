<?php
/**
 * Created by PhpStorm.
 * User: ram
 * Date: 29/8/17
 * Time: 8:21 AM
 */

namespace App\Model;

/**
 * Class DealStatus.
 */

class DealStatus
{
    const DRAFT = 'draft';
    const ONLINE = 'online';
    const OPEN = 'open';
    const CLOSED = 'closed';
    const BOUGHT = 'bought';
    const SENT = 'sent';
    const WINNER_DRAWN = 'winner_drawn';
    const PENDING = 'pending';

    public static function getApplicableStatusForGallery(): array
    {
        return [
            static::ONLINE
        ];
    }

    public static function getAllChoices(): array
    {
        return [
            static::DRAFT,
            static::ONLINE,
            static::BOUGHT,
            static::OPEN,
            static::CLOSED,
            static::SENT,
            static::WINNER_DRAWN,
            static::PENDING
        ];
    }

    public static function getAllChoicesForForm(): array
    {
        return [
            'Draft' => static::DRAFT,
            'Online' => static::ONLINE,
            'Bought' => static::BOUGHT,
            'Open' => static::OPEN,
            'Closed' => static::CLOSED,
            'Sent' => static::SENT,
            'Winner Drawn' => static::WINNER_DRAWN
        ];
    }

    public static function getOnlyOnlineDeals(): array
    {
        return [
            static::ONLINE,
            static::OPEN,
            static::WINNER_DRAWN,
            static::BOUGHT
        ];
    }
}