// assets/js/app.js
// ...

// var $ = require('jquery');
var $ = require('jquery');
window.$ = $;
window.jQuery = $;
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});

require('../js/jquery.min.js');
require('../js/jquery-ui.min.js');
require('../js/main.js');

// ... the rest of your JavaScript...